import Component from "can-component";
import DefineMap from "can-define/map/";
// import Session from "~/models/session";
// import User from "~/models/user";

import view from "./dashboard.stache";


export const ViewModel = DefineMap.extend({
  // app:  "any"
});

export default Component.extend({
  tag: "x-dashboard",
  ViewModel,
  view
});
