import loader from "@loader";
import baseMap from "can-connect/can/base-map/";
import DefineMap from "can-define/map/map";
import DefineList from "can-define/list/list";

// var connect = require("can-connect");
import canAjax from "can-ajax";
// var DefineMap = require("can-define/map/map");
// var DefineList = require("can-define/list/list");
// var tag = require('can-connect/can/tag/');
import User from "./user";


const Session = DefineMap.extend("Session", {
  user: User,
  jwt: "string",
  isAuthorized: function () {
    return true;
    // return this.user && this.user.isAdmin;
  }
});

Session.List = DefineList.extend("SessionList", {"#": Session});

Session.connection = baseMap({
  ajax: canAjax, // use jquery for parsing json objects directly into responseJson
  Map: Session,
  List: Session.List,
  url: "//"+loader.serviceBaseURL+"/api/auth/login"
});

export default Session;