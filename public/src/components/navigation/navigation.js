import Component from "can-component";
import DefineMap from "can-define/map/";
import view from "./navigation.stache";


export const ViewModel = DefineMap.extend("NavigationVM",
  {
    app:  "any",
  });

export default Component.extend({
  tag: "x-navigation",
  ViewModel,
  view
});
