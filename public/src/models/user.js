import baseMap from "can-connect/can/base-map/";
import DefineMap from "can-define/map/map";
import DefineList from "can-define/list/list";
import set from "can-set";


const User = DefineMap.extend("User", {
  id: "number",
  username: "string",
  password: "string",
  firstname: "string",
  lastname: "string",
  email: "string",
  role: "string"
});

User.List = DefineList.extend("UserList", {"#": User});

User.algebra = new set.Algebra(
  new set.Translate("where","where")
);

User.connection = baseMap({
  Map: User,
  List: User.List,
  url: {
    resource: "/services/users"
    // contentType: "application/x-www-form-urlencoded"
  },
  name: "user",
  algebra: User.algebra
});

// tag("user-model", User.connection);

export default User;