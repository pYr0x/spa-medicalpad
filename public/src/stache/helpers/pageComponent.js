import stache from "can-stache";
import "can-stache-bindings";

stache.addHelper("pageComponent", function(scope, options){
  let pageComponent = options.context.pageComponentConfig,
      helpers = scope.templateContext.helpers,
      template =
        "<can-dynamic-import from='medicalpad/components/" + pageComponent.moduleName + "'>" +
          "{{#if isResolved}}" +
            "{{#with scope.root}}<"+pageComponent.componentName + " " + pageComponent.attributes + "/>{{/with}}" +
          "{{else}}" +
            "Loading..." +
          "{{/if}}" +
        "</can-dynamic-import>";
  return stache(template)(scope, helpers, options.nodeList);
});