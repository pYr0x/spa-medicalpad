const stealTools = require("steal-tools");

// const buildPromise = stealTools.build({
//   config: __dirname + "/package.json!npm"
// }, {
//   bundleAssets: false
// });

const buildPromise = stealTools.bundle({
  config: __dirname + "/package.json!npm"
}, {
  // filter: ["node_modules/**/*", "package.json"]
  filter: ["node_modules/**/*"]
});