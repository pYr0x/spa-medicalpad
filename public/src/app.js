import steal from "@loader";
import canAjax from "can-ajax";
import DefineMap from "can-define/map/";
import DefineList from "can-define/list/list";
import route from "can-route";
// import "can-route-pushstate";
import "can-stache-route-helpers";
import "can-component";

import Cookie from "js-cookie";

// import 'can-debug#?./is-dev';


import "./stache/helpers";
import "~/components/login/";

import currentLang from "./locale/switch";
import i18n from "~/locale/#{medicalpad/locale/switch}";


import Signal from "./models/logistics/signal";
import KilometersTravelled from "./models/logistics/kilometers-travelled";



const AppViewModel = DefineMap.extend({
  init: function () {
    // console.log("test");
    // readCookie();

    // this.bind(this.lang, function () {});
  },
  env: {
    serialize: false,
    default: () => ({NODE_ENV:"development"})
  },
  message: {
    serialize: false,
    default: "Hello World!"
  },
  page: "string",
  get title(){
    return "Medicalpad | " + this.pageComponentConfig.title;
  },
  currentLang: {
    serialize: false,
    default: function () {
      return currentLang;
    }
  },
  translationList: {
    serialize: false,
    value: function (prop) {
      prop.resolve(i18n);

      prop.listenTo("currentLang", () => {
        console.log("change to ", this.currentLang);
        steal.import("~/locale/"+this.currentLang).then(function (moduleOrPlugin) {
          const i18nNew = typeof moduleOrPlugin === "function" ? moduleOrPlugin : moduleOrPlugin["default"];
          prop.resolve(i18nNew);
        });
      });
    }
  },
  session: {
    serialize: false,
    default: function() {
    //   if(Cookie.get("jwt")){
    //     Session.get({}).then((session) => {
    //       this.session = session;
    //     }, () => {
    //       this.session = null;
    //     });
    //   }else{
    //     this.session = null;
    //   }
    },
    set: function (newVal) {
      const session = newVal;
      canAjax.ajaxSetup({
        beforeSend: function (xhr){
          if(session && session.jwt){
            xhr.setRequestHeader("Authorization", "Bearer "+session.jwt);
          }
        }
      });
      return session;
    }
  },
  get pageComponentConfig() {

    if(!this.session){ // no session exists -> user not logged in
      route.data.page = "login";
      return {
        title: "Login"
      };
    }

    // if(this.session){

    if (this.page === "dashboard") {
      return {
        title: "Dashboard",
        componentName: "x-dashboard",
        attributes: "",
        moduleName: "dashboard/dashboard"
      };
      // }
    // } else {
    //   return {
    //     title: "Login",
    //     componentName: "x-login",
    //     attributes: "",
    //     moduleName: "login/login"
    //   };
    }

    return {
      title: "Page Not Found",
      componentName: "four-0-four",
      attributes: "",
      moduleName: "404.component!",
      statusCode: 404
    };

    // if (this.page === "dashboard") {
    //   return {
    //     title: "Dashboard",
    //     componentName: "x-dashboard",
    //     attributes: "",
    //     moduleName: "dashboard/dashboard"
    //   };
    // } else {
    //   return {
    //     title: "Page Not Found",
    //     componentName: "four-0-four",
    //     attributes: "",
    //     moduleName: "404.component!",
    //     statusCode: 404
    //   };
    // }
  },
  bar: function(){
    KilometersTravelled.getList({filter_period: "today"}).then((r) => {
      console.log(r);
      this.list.replace(r);
    }, function (e) {
      console.log(e);
    });
  },
  list: {
    serialize: false,
    Default: DefineList
  }
});

route.register("{page}", {page: "login"});

export default AppViewModel;
