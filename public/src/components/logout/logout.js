import Component from "can-component";
import DefineMap from "can-define/map/";
import route from "can-route";
// import Session from "~/models/session";
// import User from "~/models/user";

import view from "./logout.stache";


export const ViewModel = DefineMap.extend("LogoutVM", {
  app:  "any",
  logout: function(){
    // this.app.page = "login";
    this.app.session.destroy();
    this.app.session = null;
    // route.data.update( { page: "login"} );

  }
});

export default Component.extend({
  tag: "x-logout",
  ViewModel,
  view
});
