module.exports = {
  "env": {
    "browser": true,
    "commonjs": true,
    "es6": true,
    "mocha": true,
    "qunit": true
  },
  "extends": "eslint:recommended",
  "parserOptions": {
    "ecmaVersion": 6,
    "sourceType": "module"
  },
  "rules": {
    "indent": [
      "error",
      2,
      {
        "MemberExpression": "off",
        "VariableDeclarator": { "var": 2, "let": 2, "const": 3 }
      }
    ],
    "linebreak-style": [
      "error",
      "unix"
    ],
    "quotes": [
      "error",
      "double"
    ],
    "semi": [
      "error",
      "always"
    ],
    "no-var": ["error"],
    "comma-dangle": ["error", "never"],
    "array-bracket-spacing": ["error", "never"],
    "no-trailing-spaces": ["error", { "skipBlankLines": false }],
    "no-whitespace-before-property": ["error"],
    "prefer-const": ["error", {
      "destructuring": "any",
      "ignoreReadBeforeAssign": false
    }]
  }
};
