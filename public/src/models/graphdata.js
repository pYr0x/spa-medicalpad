import baseMap from "can-connect/can/base-map/";
import DefineMap from "can-define/map/map";
import DefineList from "can-define/list/list";


const Graphdata = DefineMap.extend("Graphdata", {
  sop: "string"
});

Graphdata.List = DefineList.extend("GraphdataList", {"#": Graphdata});

// Graphdata.algebra = new set.Algebra(
//   new set.Translate("where","where"),
//   set.comparators.sort("sortBy")
// );

Graphdata.connection = baseMap({
  Map: Graphdata,
  List: Graphdata.List,
  url: "http://localhost/jwt/foo.php",
  name: "Graphdata"
});


export default Graphdata;