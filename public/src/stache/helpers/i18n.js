import stache from "can-stache";
import fallBackTranslationList from "~/locale/de";


stache.addHelper("i18n", function (str) {
  const translationList = arguments[1].scope.root.translationList;
  if(translationList.hasOwnProperty(str) === false && fallBackTranslationList.hasOwnProperty(str) === false){
    return "####";
  }else if(translationList.hasOwnProperty(str) === false){
    return fallBackTranslationList[str];
  }
  return translationList[str];
});