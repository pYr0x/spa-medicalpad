const express = require('express');
const axios = require("axios");

const router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  // res.render('index', {title: 'Express'});
  res.send("hallo");
  // res.status(401).json({message: "Incorrect username or password"});
});

async function getJWT() {
  try {
    const response = await axios.get("http://localhost/jwt/index.php");
    const data = response.data;

  } catch (error) {
    console.log(error);
  }
};

router.post('/', async function (req, res, next) {
  res.send(await getJWT() + req.body.username);
});

module.exports = router;
