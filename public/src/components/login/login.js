import Component from "can-component";
import DefineMap from "can-define/map/";
import route from "can-route";

import Session from "~/models/session";
import User from "~/models/user";

import view from "./login.stache";


export const ViewModel = DefineMap.extend({
  app:  "any",
  sessionPromise: "any",
  /*
   * A placeholder session with a nested [user] property that
   * is used for two-way binding the login form's username and password.
   */
  loginSession: {
    default: function(){
      return new Session({user: new User()});
    }
  },
  createSession: function(ev){
    if(ev) {
      ev.preventDefault();
    }
    this.sessionPromise = this.loginSession.save()
                              .then((session) => {
                                this.loginSession     = new Session({user: new User()}); // clear all user credentials
                                session.user.password = ""; // clear all user credentials

                                route.data.page  = "dashboard";
                                this.app.session = session;

                              });
                              // .catch((response) => {
                              //   // return Promise.reject(response);
                              //   return response;
                              //   console.log(response);
                              // });
  }
});

export default Component.extend({
  tag: "x-login",
  ViewModel,
  view
});
