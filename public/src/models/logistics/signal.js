import baseMap from "can-connect/can/base-map/";
import DefineMap from "can-define/map/map";
import DefineList from "can-define/list/list";


const Signal = DefineMap.extend("Signaldata", {
  value: "string"
});

Signal.List = DefineList.extend("SignaldataList", {"#": Signal});

Signal.connection = baseMap({
  Map: Signal,
  List: Signal.List,
  url: {
    // getListData: "GET http://localhost/medicalpad/api/logistics/signal/{graph}/graphdata",
    getData: "GET http://medicalpad.local/api/logistics/signal/{graph}/graphdata",
    // createData: "POST /todos",
    // updateData: "PUT /todos/{id}",
    // destroyData: "DELETE /todos/{id}"
  },
  name: "Signaldata"
});


export default Signal;