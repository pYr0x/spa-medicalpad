import Cookie from "js-cookie";

const locales = ["de", "en"];

let lang = Cookie.get("lang");
if(lang === undefined || locales.indexOf(lang) === false){
  Cookie.set("lang", "de");
  lang = "de";
}

export default lang;