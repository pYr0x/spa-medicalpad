import loader from "@loader";
import baseMap from "can-connect/can/base-map/";
import DefineMap from "can-define/map/map";
import DefineList from "can-define/list/list";
import set from "can-set";

const KilometersTravelled = DefineMap.extend("KilometersTravelled", {
  label: "string",
  value: "number"
});

KilometersTravelled.List = DefineList.extend("KilometersTravelledList", {"#": KilometersTravelled});

const algebra = new set.Algebra({
  filter_organization: () => true,
  filter_station: () => true,
  filter_period: () => true
});

KilometersTravelled.connection = baseMap({
  Map: KilometersTravelled,
  List: KilometersTravelled.List,
  url: {
    getListData: "GET //"+loader.serviceBaseURL+"/api/logistics/kilometers-travelled/graphdata"
    // getData: "GET http://medicalpad.local/api/logistics/signal/{graph}/graphdata",
    // createData: "POST /todos",
    // updateData: "PUT /todos/{id}",
    // destroyData: "DELETE /todos/{id}"
  },
  name: "KilometersTravelled",
  algebra: algebra
});


export default KilometersTravelled;